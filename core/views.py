from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from oauth2_provider.models import AccessToken
from django.dispatch import receiver
from django.db.models.signals import post_save
from datetime import datetime
import json

from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters
from oauth2_provider.models import get_access_token_model
from oauth2_provider.signals import app_authorized
from oauth2_provider.views import TokenView
from rest_framework import viewsets, permissions, pagination

from core import serializers
from core.constant import constants


@receiver(post_save, sender=AccessToken, dispatch_uid="record_last_login")
def record_login(sender, instance, created, **kwargs):
    if created:
        instance.user.last_login = datetime.now()
        instance.user.save()


class CustomView(TokenView):
    @method_decorator(sensitive_post_parameters("password"))
    def post(self, request, *args, **kwargs):
        url, headers, body, status = self.create_token_response(request)
        if status == 200:
            access_token = json.loads(body).get("access_token")
            if access_token is not None:
                token = get_access_token_model().objects.get(
                    token=access_token)
                app_authorized.send(
                    sender=self, request=request,
                    token=token)
                token.user.userlogin_set.create(timestamp=token.user.last_login)
        response = HttpResponse(content=body, status=status)

        for k, v in headers.items():
            response[k] = v
        return response


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = serializers.UserSerializer
    queryset = serializer_class.Meta.model.objects.all()
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    pagination_class = pagination.LimitOffsetPagination
    ordering_fields = ['-created_at']
    lookup_field = 'uid'

    def get_queryset(self, *args, **kwargs):
        _user_obj = self.request.user
        queryset = super().get_queryset()

        if _user_obj.is_anonymous:
            return queryset.none()

        if not _user_obj.type == constants.ADMIN_ID:
            return queryset
        elif _user_obj.type in [constants.EMPLOYEE_ID, constants.RESTAURANT_ID]:
            return queryset.filter(user=_user_obj)

        return queryset.none()

