from django.utils.translation import ugettext_lazy as _

# API version
API_V1 = 'v1'

# Unique field name
UUID_FIELD_NAME = 'uid'

# User types
ADMIN_ID, MANAGER_ID, EMPLOYEE_ID, RESTAURANT_ID = 1, 2, 3, 4
ADMIN, MANAGER, EMPLOYEE, RESTAURANT = _('Admin'), _('Manager'), _('Employee'), _('Restaurant')
USER_TYPES = (
    (ADMIN_ID, ADMIN),
    (MANAGER_ID, MANAGER),
    (EMPLOYEE_ID, EMPLOYEE),
    (RESTAURANT_ID, RESTAURANT)
)
