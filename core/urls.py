
from django.urls import path
from rest_framework import routers

from . import views

router = routers.SimpleRouter()
router.register('api/v1', views.UserViewSet, 'users')

urlpatterns = [
    path(
        'api/v1/token/',
        views.CustomView.as_view(),
        name='CustomTokenView'
    ),
]

urlpatterns += router.urls
