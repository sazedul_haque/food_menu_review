from django.db import transaction
from django.utils import timezone
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import empty
from rest_framework.utils.html import is_html_input
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model

from core.constant import UUID_FIELD_NAME

__all__ = [
    'BaseModelSerializer',
    'related_serializer_process',
    'UserSerializer',
]


def related_serializer_process(related_serializer, data, context, instance=None):
    if instance:
        serializer = related_serializer(instance, data=data, context=context)
    else:
        serializer = related_serializer(data=data, context=context)
    serializer.is_valid(raise_exception=True)
    serializer.save()

    return serializer.instance


class BaseModelSerializer(serializers.ModelSerializer):

    def __init__(self, instance=None, data=empty, **kwargs):
        self.bind_with_uid = kwargs.pop('bind_with_uid', False)
        super().__init__(instance, data, **kwargs)

    def validate_empty_values(self, data):
        if self.bind_with_uid and data not in (empty, None):
            key = 'pk' if isinstance(data, int) else UUID_FIELD_NAME
            model = getattr(self.Meta, 'model')
            try:
                instance = model.objects.get(**{key: data})
                return True, instance
            except model.DoesNotExist:
                raise ValidationError(_('Invalid primary key'), code='does_not_exist')
            except (ValueError, ValidationError):
                raise ValidationError(_('Invalid data. Expect int or uuid4'))
        return super().validate_empty_values(data)

    def _has_fields(self, *fields):
        set_fields = set(fields)
        return set([f.name for f in self.Meta.model._meta.fields]).intersection(set_fields) == set_fields

    def _has_update_fields(self):
        return self._has_fields('updated_by', 'updated_at')

    def _has_create_fields(self):
        return self._has_fields('created_by', 'created_at')

    def get_fields(self):
        fields = super().get_fields()
        # if self._has_update_fields():
        #     fields['updated_by'] = UserSerializer(read_only=True, context=self.context)
        #     fields['updated_at'] = serializers.DateTimeField(read_only=True)
        # if 'created_by' not in fields and self._has_create_fields():
        #     fields['created_by'] = UserSerializer(read_only=True, context=self.context)
        #     fields['created_at'] = serializers.DateTimeField(read_only=True)
        return fields

    def get_field_names(self, declared_fields, info):
        fields = list(super().get_field_names(declared_fields, info))
        model_fields = [f.name for f in self.Meta.model._meta.fields]
        if UUID_FIELD_NAME not in fields and UUID_FIELD_NAME in model_fields:
            if isinstance(fields, list):
                fields.append(UUID_FIELD_NAME)
            else:
                fields = fields + (UUID_FIELD_NAME,)
        return tuple(fields)

    def get_value(self, dictionary):
        if self.bind_with_uid and is_html_input(dictionary):
            return dictionary.get(self.field_name, empty)
        return super().get_value(dictionary)

    @property
    def validated_data(self):
        validated_data = super().validated_data
        request = self.context.get('request', None)
        if request:
            user = request.user if request.user.is_authenticated else None
            if self.instance and self._has_update_fields():
                validated_data['updated_by'] = user
                validated_data['updated_at'] = timezone.now()

            if not self.instance and self._has_create_fields():
                validated_data['created_by'] = user
        return validated_data


class UserSerializer(BaseModelSerializer):
    def validate_empty_values(self, data):
        try:
            data['username'] = data['email']

        except (AttributeError, KeyError, IndexError) as e:
            pass
        return super().validate_empty_values(data)

    @transaction.atomic
    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance

    class Meta:
        model = get_user_model()
        read_only_fields = ('id', 'uid')
        fields = read_only_fields + (
            'first_name',
            'last_name',
            'username',
            'email',
            'type',
            'password',
        )
        extra_kwargs = {
            "password":
                {
                    "write_only": True
                },
            "username":
                {
                    "write_only": True
                },
            "type":
                {
                    "write_only": True
                }
        }
