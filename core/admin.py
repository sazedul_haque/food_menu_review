from django.contrib import admin

from core import models


class UserAdmin(admin.ModelAdmin):
    readonly_fields = ('uid',)


admin.site.register(models.User, UserAdmin)
