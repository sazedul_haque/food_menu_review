from rest_framework import routers

from apps.company import views

router = routers.SimpleRouter()
router.register('v1/company', views.CompanyViewSet)
router.register('v1/employee', views.EmployeeViewSet)
urlpatterns = router.urls
