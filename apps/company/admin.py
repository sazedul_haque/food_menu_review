from django.contrib import admin
from . import models


@admin.register(models.Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = [
        'address',
        'company_name',
    ]


@admin.register(models.Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = [
        'active',
        'address',
        'company',
        'user',
        'phone',
    ]
