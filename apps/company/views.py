import logging

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework import permissions, viewsets, pagination
from rest_framework.response import Response
from django.utils.translation import ugettext_lazy as _

from apps.company import serializers
from core.constant import constants

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class CompanyBaseViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAdminUser]
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    pagination_class = pagination.LimitOffsetPagination
    ordering_fields = ['-created_at']
    lookup_field = 'uid'


class CompanyViewSet(CompanyBaseViewSet):
    serializer_class = serializers.CompanySerializer
    queryset = serializer_class.Meta.model.objects.all()


class EmployeeViewSet(CompanyBaseViewSet):
    serializer_class = serializers.EmployeeSerializer
    queryset = serializer_class.Meta.model.objects.all()

    def get_queryset(self, *args, **kwargs):
        _user_obj = self.request.user
        queryset = super().get_queryset()

        if _user_obj.is_anonymous:
            return queryset.none()

        if _user_obj.type == constants.ADMIN_ID:
            return queryset
        elif _user_obj.type == constants.EMPLOYEE_ID:
            return queryset.filter(user=_user_obj)

        return queryset.none()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance:
            if instance.user:
                instance.user.delete()
            instance.delete()
            return Response(_('deleted successfully'), status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(_('Data Not found'), status=status.HTTP_302_FOUND)
