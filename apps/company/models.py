from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models import BaseModel


class Company(BaseModel):
    company_name = models.CharField(
        verbose_name=_('Company name'),
        max_length=200,
        null=True,
        blank=True
    )
    address = models.ForeignKey(
        to='address.AddressInfo',
        verbose_name=_('company address'),
        related_name='company_address',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self):
        return self.company_name or self.uid

    class Meta:
        ordering = ['-created_at']


class Employee(BaseModel):
    address = models.ForeignKey(
        to='address.AddressInfo',
        verbose_name=_('Employee address'),
        related_name='employee_address',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    company = models.ForeignKey(
        to='Company',
        verbose_name=_('Company'),
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )
    user_model = get_user_model()
    user = models.OneToOneField(
        user_model,
        on_delete=models.CASCADE,
        related_name='company_user',
        null=True,
        blank=True
    )
    phone = models.CharField(
        max_length=15,
        null=True,
        blank=True
    )
    active = models.BooleanField(
        default=True
    )

    def __str__(self):
        return f"ID#{self.pk}"

    class Meta:
        ordering = ['-created_at']
