from django.db import transaction

from apps.address.serializers import AddressSerializer
from apps.company import models
from core.serializers import BaseModelSerializer, related_serializer_process, UserSerializer


class CompanySerializer(BaseModelSerializer):
    address = AddressSerializer(allow_null=True, required=False)

    @transaction.atomic
    def create(self, validated_data):
        address_data = validated_data.pop('address')
        address_serializer = related_serializer_process(
            AddressSerializer,
            address_data,
            self.context
        )
        validated_data['address'] = address_serializer

        instance = super().create(validated_data)
        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        if 'address' in validated_data:
            address_data = validated_data.pop('address')
            address_obj = related_serializer_process(
                AddressSerializer,
                address_data,
                self.context,
                instance=instance.address
            )
            validated_data['address'] = address_obj

        return super().update(instance, validated_data)

    class Meta:
        model = models.Company
        read_only_fields = ('id', 'uid')
        fields = read_only_fields + (
            'company_name',
            'address'
        )
        extra_kwargs = {'url': {'lookup_field': 'uid'}}


class EmployeeSerializer(BaseModelSerializer):
    user = UserSerializer(required=True)
    company = CompanySerializer(bind_with_uid=True)
    address = AddressSerializer(allow_null=True, required=False)

    @transaction.atomic
    def create(self, validated_data):
        if 'address' in validated_data:
            address_data = validated_data.pop('address')
            address_serializer = related_serializer_process(
                AddressSerializer,
                address_data,
                self.context
            )
            validated_data['address'] = address_serializer
        user_data = validated_data.pop('user')
        user_data['username'] = user_data['email']
        user_serializer = related_serializer_process(
            UserSerializer,
            user_data,
            self.context
        )
        validated_data['user'] = user_serializer

        instance = super().create(validated_data)
        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        if 'address' in validated_data:
            address_data = validated_data.pop('address')
            address_obj = related_serializer_process(
                AddressSerializer,
                address_data,
                self.context,
                instance=instance.address
            )
            validated_data['address'] = address_obj
        if 'user' in validated_data:
            user_data = validated_data.pop('user')
            user_obj = related_serializer_process(
                UserSerializer,
                user_data,
                self.context,
                instance=instance.user
            )
            validated_data['user'] = user_obj

        return super().update(instance, validated_data)

    class Meta:
        model = models.Employee
        read_only_fields = ('id', 'uid')
        fields = read_only_fields + (
            'company',
            'address',
            'user',
            'phone',
        )
        extra_kwargs = {'url': {'lookup_field': 'uid'}}
