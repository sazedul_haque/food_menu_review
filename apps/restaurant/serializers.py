from django.db import transaction

from apps.address.serializers import AddressSerializer
from apps.restaurant import models
from core import constant
from core.serializers import BaseModelSerializer, related_serializer_process, UserSerializer


class RestaurantSerializer(BaseModelSerializer):
    address = AddressSerializer(allow_null=True, required=False)

    @transaction.atomic
    def create(self, validated_data):
        address_data = validated_data.pop('address')
        address_serializer = related_serializer_process(
            AddressSerializer,
            address_data,
            self.context
        )
        validated_data['address'] = address_serializer

        instance = super().create(validated_data)
        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        if 'address' in validated_data:
            address_data = validated_data.pop('address')
            address_obj = related_serializer_process(
                AddressSerializer,
                address_data,
                self.context,
                instance=instance.address
            )
            validated_data['address'] = address_obj

        return super().update(instance, validated_data)

    class Meta:
        model = models.Restaurant
        read_only_fields = ('id', 'uid')
        fields = read_only_fields + (
            'restaurant_name',
            'address'
        )
        extra_kwargs = {'url': {'lookup_field': 'uid'}}


class RestaurantSortSerializer(BaseModelSerializer):
    class Meta:
        model = models.Restaurant
        read_only_fields = ('uid',)
        fields = read_only_fields + (
            'restaurant_name',
        )
        extra_kwargs = {'url': {'lookup_field': 'uid'}}


class RestaurantEmployeeSerializer(BaseModelSerializer):
    user = UserSerializer(required=True)
    restaurant = RestaurantSerializer(bind_with_uid=True)

    @transaction.atomic
    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user_data['type'] = constant.RESTAURANT_ID
        user_serializer = related_serializer_process(
            UserSerializer,
            user_data,
            self.context
        )
        validated_data['user'] = user_serializer

        instance = super().create(validated_data)
        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        if 'user' in validated_data:
            user_data = validated_data.pop('user')
            user_obj = related_serializer_process(
                UserSerializer,
                user_data,
                self.context,
                instance=instance.user
            )
            validated_data['user'] = user_obj

        return super().update(instance, validated_data)

    class Meta:
        model = models.RestaurantEmployee
        read_only_fields = ('id', 'uid')
        fields = read_only_fields + (
            'restaurant',
            'user',
            'phone',
        )
        extra_kwargs = {'url': {'lookup_field': 'uid'}}


class MealSerializer(BaseModelSerializer):
    restaurant = RestaurantSortSerializer(bind_with_uid=True)

    @transaction.atomic
    def create(self, validated_data):
        instance = super().create(validated_data)
        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        return super().update(instance, validated_data)

    class Meta:
        model = models.Meal
        read_only_fields = ('id', 'uid', 'serve_date')
        fields = read_only_fields + (
            'restaurant',
            'title',
            'Meal_in_text',
            'price',
            'image',
        )
        extra_kwargs = {'url': {'lookup_field': 'uid'}}
