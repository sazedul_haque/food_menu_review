from django.contrib.auth import get_user_model
from django.db import models

# Create your models here.
from image_cropping import ImageCropField, ImageRatioField

from core.models import BaseModel
from django.utils.translation import ugettext_lazy as _


def get_meal_image_folder(filename):
    return f"restaurant/{filename}"


class Restaurant(BaseModel):
    restaurant_name = models.CharField(
        verbose_name=_('Restaurant name'),
        max_length=200,
        null=True,
        blank=True
    )
    address = models.ForeignKey(
        to='address.AddressInfo',
        verbose_name=_('Restaurant address'),
        related_name='restaurant_address',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self):
        return self.restaurant_name or self.uid

    class Meta:
        ordering = ['-created_at']


class RestaurantEmployee(BaseModel):
    user_model = get_user_model()
    user = models.OneToOneField(
        user_model,
        on_delete=models.CASCADE,
        related_name='restaurant_user',
        null=True,
        blank=True
    )
    restaurant = models.ForeignKey(
        to='Restaurant',
        verbose_name=_('Restaurant'),
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )
    phone = models.CharField(
        max_length=15,
        null=True,
        blank=True
    )
    active = models.BooleanField(
        default=True
    )

    def __str__(self):
        return f"ID#{self.pk} ({self.first_name} {self.last_name})"

    class Meta:
        ordering = ['-created_at']


class Meal(BaseModel):
    title = models.CharField(
        max_length=200,
        null=True,
        blank=True
    )
    restaurant = models.ForeignKey(
        to='Restaurant',
        verbose_name=_('Restaurant'),
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    Meal_in_text = models.TextField(
        verbose_name=_('Meal details'),
        null=True,
        blank=True
    )
    price = models.DecimalField(
        decimal_places=6,
        max_digits=20,
        null=True,
        blank=True
    )
    image = ImageCropField(
        blank=True,
        null=True,
        upload_to=get_meal_image_folder,
    )
    image_cropping = ImageRatioField(
        'image',
        '800x600'
    )
    serve_date = models.DateField(
        auto_now_add=True,
        editable=False,
        null=True,
    )

    def __str__(self):
        return self.uid

    class Meta:
        ordering = ['-created_at']
