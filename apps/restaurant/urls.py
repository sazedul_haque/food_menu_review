from rest_framework import routers

from apps.restaurant import views

router = routers.SimpleRouter()
router.register('v1/restaurant', views.RestaurantViewSet)
router.register('v1/restaurant-user', views.RestaurantEmployeeViewSet)
router.register('v1/meal', views.MealViewSet)
router.register('v1/current-day-meal', views.CurrentDateMeal)
urlpatterns = router.urls
