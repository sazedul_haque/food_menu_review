import datetime
import logging

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework import permissions, viewsets, pagination
from rest_framework.response import Response
from django.utils.translation import ugettext_lazy as _

from apps.restaurant import serializers
from core.constant import constants

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class RestaurantBaseViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    pagination_class = pagination.LimitOffsetPagination
    ordering_fields = ['-created_at']
    lookup_field = 'uid'

    def get_queryset(self, *args, **kwargs):
        _user_obj = self.request.user
        queryset = super().get_queryset()

        if _user_obj.is_anonymous:
            return queryset.none()

        if _user_obj.type == constants.ADMIN_ID:
            return queryset
        else:
            return queryset.filter(created_by=_user_obj)


class RestaurantViewSet(RestaurantBaseViewSet):
    serializer_class = serializers.RestaurantSerializer
    queryset = serializer_class.Meta.model.objects.all()


class RestaurantEmployeeViewSet(RestaurantBaseViewSet):
    permission_classes = [permissions.IsAdminUser]
    serializer_class = serializers.RestaurantEmployeeSerializer
    queryset = serializer_class.Meta.model.objects.all()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance:
            if instance.user:
                instance.user.delete()
            instance.delete()
            return Response(_('deleted successfully'), status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(_('Data Not found'), status=status.HTTP_302_FOUND)


class MealViewSet(RestaurantBaseViewSet):
    serializer_class = serializers.MealSerializer
    queryset = serializer_class.Meta.model.objects.all()
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    ordering_fields = ['-created_on']
    filterset_fields = ['serve_date']
    search_fields = (
        'title',
        'restaurant__restaurant_name',
        'Meal_in_text',
        'price',
    )

    def get_queryset(self, *args, **kwargs):
        _user_obj = self.request.user
        queryset = super().get_queryset()

        if _user_obj.is_anonymous:
            return queryset.none()

        if _user_obj.type == constants.ADMIN_ID or _user_obj.type == constants.EMPLOYEE_ID:
            return queryset
        elif _user_obj.type == constants.RESTAURANT_ID:
            return queryset.filter(created_by=_user_obj)
        else:
            return queryset.none()


class CurrentDateMeal(RestaurantBaseViewSet):
    serializer_class = serializers.MealSerializer
    queryset = serializer_class.Meta.model.objects.all()
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    ordering_fields = ['-created_on']
    filterset_fields = ['serve_date']
    search_fields = (
        'title',
        'restaurant__restaurant_name',
        'Meal_in_text',
        'price',
    )

    def get_queryset(self, *args, **kwargs):
        _user_obj = self.request.user
        queryset = super().get_queryset()

        if _user_obj.is_anonymous:
            return queryset.none()

        if _user_obj.type == constants.ADMIN_ID or _user_obj.type == constants.EMPLOYEE_ID:
            return queryset.filter(serve_date=datetime.datetime.now().date())
        elif _user_obj.type == constants.RESTAURANT_ID:
            return queryset.filter(serve_date=datetime.datetime.now().date(), created_by=_user_obj)
        else:
            return queryset.none()
