from django.urls import path
from rest_framework.routers import SimpleRouter

from . import views

router = SimpleRouter()

router.register('v1/rating', views.RatingInsertAPIView)

# URLs
urlpatterns = [
    path('v1/current-day-rating', views.RatingByCurrentDayMealAPIView.as_view()),

]

urlpatterns += router.urls
