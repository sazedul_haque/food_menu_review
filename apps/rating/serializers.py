from django.utils.translation import ugettext_lazy as _
from rest_framework.serializers import ModelSerializer, ValidationError
from rest_framework import serializers
import logging
from . import models
from ..restaurant.serializers import MealSerializer

logger = logging.getLogger(__name__)


class RatingSerializer(ModelSerializer):
    meal = MealSerializer(bind_with_uid=True, many=False)
    # meal_uid = serializers.CharField(source='meal.uid', allow_blank=True, allow_null=True)

    class Meta:
        model = models.Rating
        fields = [
            'meal',
            # 'meal_uid',
            'star',
            'comment',
            'helpful',
        ]


class RatingInsertSerializer(ModelSerializer):

    def validate(self, data):
        user_obj = self.context['request'].user
        error_message = ()

        if len(error_message) > 0:
            raise ValidationError(error_message)
        return data

    class Meta:
        model = models.Rating
        fields = [
            'meal',
            'star',
            'comment',
            'helpful',
        ]
