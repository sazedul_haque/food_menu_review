from django.utils.translation import ugettext_lazy as _
# from meals.models import Meal
from django.db import models

from apps.restaurant.models import Meal
from core.models import BaseModel


class Rating(BaseModel):
    meal = models.ForeignKey(
        Meal,
        models.CASCADE,
        related_name="ratings",
    )
    star = models.FloatField(
        # blank=True,
        # null=True
    )
    comment = models.TextField(
        blank=True,
        null=True
    )
    helpful = models.BooleanField(
        blank=True,
        null=True,
        default=None
    )

    # Meta
    class Meta:
        ordering = ['-created_at']
        verbose_name = _('Rating')
        verbose_name_plural = _('Ratings')
    #
    def __str__(self):
        return f"Meal#:{self.meal and self.meal.title}- rating is:{self.star and self.star or None} "
