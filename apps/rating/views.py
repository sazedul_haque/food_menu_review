import datetime

from django.db.models import Sum, Avg, Count
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import permissions, filters, status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from core.constant import constants
from .models import *
from . import serializers
import logging

logger = logging.getLogger(__name__)


# Views
class RatingInsertAPIView(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = serializers.RatingSerializer
    queryset = serializer_class.Meta.model.objects.all()
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    ordering_fields = ['-created_on']
    search_fields = (
        'meal__title',
        'meal__restaurant__restaurant_name',
        'meal__Meal_in_text',
        'meal__price',
        'star',
    )

    def get_queryset(self, *args, **kwargs):
        _user_obj = self.request.user
        queryset = super().get_queryset()

        if _user_obj.is_anonymous:
            return queryset.none()

        if _user_obj.type == constants.ADMIN_ID or _user_obj.type == constants.EMPLOYEE_ID:
            return queryset
        else:
            return queryset.filter(created_by=_user_obj)


class RatingByCurrentDayMealAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @staticmethod
    def get(request, *args, **kwargs):
        user_obj = request.user
        """
        Here we are calculating the average Rating of each meal for current day
        """
        body = {}
        winner = {}

        try:
            rating_obj = Rating.objects.filter(meal__serve_date=datetime.datetime.now().date())
        except Exception as e:
            logger.error('Exception of meal__serve_date =', e)
            rating_obj = 'not set'
            winner = 'not set'

        rating_obj = rating_obj.values('meal__title').annotate(
            rating=Sum('star'),
            avg_rating=Avg('star'),
            given_by=Count('id')
        ).order_by('-rating')

        body = {
            'data': rating_obj,
            'winner': rating_obj.first(),
        }


        return Response(body, status=status.HTTP_200_OK)
        # else:
        #     return Response({'message': _('Mail not send')}, status=status.HTTP_406_NOT_ACCEPTABLE)
