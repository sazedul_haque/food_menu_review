from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models import BaseModel


class AddressInfo(BaseModel):
    line_1 = models.CharField(max_length=512, verbose_name=_('line 1'), null=True, blank=True)
    line_2 = models.CharField(max_length=512, verbose_name=_('line 2'), null=True, blank=True)
    zip_code = models.CharField(max_length=10, verbose_name=_('zip code'), null=True, blank=True)
    city = models.CharField(max_length=30, verbose_name=_('city'), null=True, blank=True)
    state = models.CharField(max_length=10, verbose_name=_('state'), null=True, blank=True)
    country = models.CharField(max_length=255, verbose_name=_('country'), default='USA')
    lat = models.CharField(max_length=255, verbose_name=_('latitude'), null=True, blank=True)
    lng = models.CharField(max_length=255, verbose_name=_('longitude'), null=True, blank=True)

    def __str__(self):
        return self.full_address

    class Meta:
        ordering = ['-created_at']

    @property
    def full_address(self):
        line_1 = f"{self.line_1}," if self.line_1 else ''
        line_2 = f" {self.line_2}," if self.line_2 else ''
        zip_code = f" {self.zip_code}," if self.zip_code else ''
        city = f" {self.city}," if self.city else ''
        state = f" {self.state}," if self.state else ''
        country = f" {self.country}"
        return f"{line_1}{line_2}{zip_code}{city}{state} {country}"
