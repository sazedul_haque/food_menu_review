# Generated by Django 3.2.4 on 2021-06-30 15:15

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AddressInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True, verbose_name='UUID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, null=True, verbose_name='updated at')),
                ('line_1', models.CharField(blank=True, max_length=512, null=True, verbose_name='line 1')),
                ('line_2', models.CharField(blank=True, max_length=512, null=True, verbose_name='line 2')),
                ('zip_code', models.CharField(blank=True, max_length=10, null=True, verbose_name='zip code')),
                ('city', models.CharField(blank=True, max_length=30, null=True, verbose_name='city')),
                ('state', models.CharField(blank=True, max_length=10, null=True, verbose_name='state')),
                ('country', models.CharField(default='USA', max_length=255, verbose_name='country')),
                ('lat', models.CharField(blank=True, max_length=255, null=True, verbose_name='latitude')),
                ('lng', models.CharField(blank=True, max_length=255, null=True, verbose_name='longitude')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='addressinfo_created', to=settings.AUTH_USER_MODEL, verbose_name='created by')),
                ('updated_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='addressinfo_updated', to=settings.AUTH_USER_MODEL, verbose_name='updated by')),
            ],
            options={
                'ordering': ['-created_at'],
            },
        ),
    ]
