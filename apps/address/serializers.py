from core.serializers import BaseModelSerializer
from apps.address import models


class AddressSerializer(BaseModelSerializer):
    class Meta:
        model = models.AddressInfo
        read_only_fields = ('id', 'uid', 'full_address')
        fields = read_only_fields + (
            'line_1',
            'line_2',
            'zip_code',
            'city',
            'state',
            'country'
        )
