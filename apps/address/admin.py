from django.contrib import admin
from apps.address import models

admin.site.register(models.AddressInfo)
