## How to run the demo
***
you need to clone the demo from given bitbucket url

```commandline
git clone https://bitbucket.org/sazedul_haque/meal_reviewer.git
```

After that you have to create the virtual environment and have to activate it.
Then run the following command
```commandline
pip install -r requirements.txt 
```
Create a file with name `/conf/.env` and copy the environment variable from ```/conf/.env.example``` put the needed info.
Create the database if not created yet and put the database info in the `/conf/.env` file and run the command bellow

```commandline
python manage.py migrate
source ./init.sh
```

* I put all the apps in `apps` folder but we can put them in root.

### Rest API Details of demo is share by the follwoing link of Postman collection
I also attached the exported json files of the collection and environment in the Repo by following JSON file name
* Meal-Reviewer.postman_collection.json
* Meal-Reviewer-ENV.postman_environment.json

[Public link of Postman Collection of Meal Reviewer Demo](https://www.postman.com/warped-robot-4921/workspace/share-to-anyone/request/395543-9ed804ba-cb27-4faa-bdaf-bf063c152ebb).

N.B. please select the environment first as its also given

### Running demo in Docker

To run the application in docker we have 2 way which is following
1. Easy way
2. Hard way

#### 1. Easy way
If you use remote postgres database then after putting the database info in `.env` run the following command to build docker images

```commandline
docker build -t mr .
```

then run the image in docker and you can use it for testing

#### 2. Hard way
If you want to use local postgres database then install or download `ngrok` apps from https://ngrok.com .
After that login to ngrok dashboard https://dashboard.ngrok.com/get-started/setup and copy and run the token code like following
```commandline
./ngrok authtoken {your-token}
```
then run
```commandline
./ngrok tcp 5432
```
it will give you following look from where you will get `url` and `port`

~~~
ngrok by @inconshreveable                                                                        (Ctrl+C to quit)
                                                                                                                 
Session Status                online                                                                             
Account                       your-email@gmail.com (Plan: Free)                                                
Version                       2.3.40                                                                             
Region                        United States (us)                                                                 
Web Interface                 http://127.0.0.1:4040                                                              
Forwarding                    tcp://4.tcp.ngrok.io:17544 -> localhost:5432                                       
                                                                                                                 
Connections                   ttl     opn     rt1     rt5     p50     p90                                        
                              8       0       0.00    0.00    32.21   487.13  
                              
~~~

putting the database info in `.env` file and run the following command to build docker images

```commandline
docker build -t mr .
```

then run the image in docker and you can use it for testing.