FROM python:3.9.6
ENV PYTHONUNBUFFERED 1
WORKDIR /code
RUN apt update && apt install build-essential python-dev wkhtmltopdf -y
COPY requirements.txt requirements.txt
COPY . /code/
RUN python -m pip install -U pip
RUN pip install -r requirements.txt
RUN python manage.py migrate
RUN ./init.sh
EXPOSE 8000
CMD gunicorn conf.wsgi --workers 4 --timeout 120 --bind 0.0.0.0:8000